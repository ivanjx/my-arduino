#include <Arduino.h>
#include <LiquidCrystal.h>

const int LED_CONTROL_PIN = 9;
const int VOLT_READER_PIN = A0;
const int BEEP_INTERVAL = 1;
const int MAX_PWM_STATE = 255;
const int LOW_PWM_STATE = 30;
const float MAX_PWM_VOLT = 3.3;
const int VOLT_READER_INTERVAL = 1000;
const int VOLT_READER_MULTIPLIER = 3;
const float VOLT_READER_ERR_CORRECTION = 0.2;
const float LOW_BATT_VOLT = 7.5;

const int LCD_RS = 2;
const int LCD_E = 3;
const int LCD_DB4 = 4;
const int LCD_DB5 = 5;
const int LCD_DB6 = 6;
const int LCD_DB7 = 7;

unsigned long _timeBeep = 0;
unsigned long _timeVoltReader = 0;

int _beepState = 0;
int _beepAdder = 1;
float _voltage = 0;

LiquidCrystal lcd (
  LCD_RS,
  LCD_E,
  LCD_DB4,
  LCD_DB5,
  LCD_DB6,
  LCD_DB7);

void beep() {
  // Setting led.
  analogWrite(LED_CONTROL_PIN, _beepState);

  // Determining next state and adder.
  int maxState;
  
  if(_voltage <= LOW_BATT_VOLT) {
    maxState = LOW_PWM_STATE;
  } else {
    maxState = MAX_PWM_STATE;
  }

  if(_beepState > maxState) {
    // Invalid condition.
    // Resetting states.
    _beepState = 0;
    _beepAdder = 1;
  } else {
    if(_beepState == maxState) {
      _beepAdder = -1;
    } else if(_beepState == 0) {
      _beepAdder = 1;
    }

    _beepState += _beepAdder;
  }
}

void updateVoltage() {
  // Reading analog value.
  int sensorValue = analogRead(VOLT_READER_PIN);

  // Converting to voltage.
  _voltage = (sensorValue * MAX_PWM_VOLT / 1023.0 + VOLT_READER_ERR_CORRECTION) * VOLT_READER_MULTIPLIER;

  // Printing voltage.
  Serial.print("Voltage: ");
  Serial.println(_voltage, 4);
}

void setup() {
  // Setting up serial baud rate.
  Serial.begin(9600);

  // Setting output pins.
  pinMode(LED_CONTROL_PIN, OUTPUT);

  // Setting up lcd.
  lcd.begin(16,4);

  // Getting current volt.
  updateVoltage();
}

void loop() {  
  lcd.print("hello, world!");
  lcd.setCursor(0,1);
  lcd.print("it works!");
  
  if(millis() > _timeBeep + BEEP_INTERVAL) {
    _timeBeep = millis();
    beep();
  }

  if(millis() > _timeVoltReader + VOLT_READER_INTERVAL) {
    _timeVoltReader = millis();
    updateVoltage();
  }
}